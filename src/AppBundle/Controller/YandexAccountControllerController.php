<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountType;
use AppBundle\Entity\MasterCard;
use AppBundle\Entity\Status;
use AppBundle\Entity\YandexReserveCode;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class YandexAccountControllerController
 * @package AppBundle\Controller
 * @Route("/yandex")
 */
class YandexAccountControllerController extends Controller
{
    /**
     * @Route("/{id}")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function getAction($id)
    {
        $user = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Account::class);
        $account = $repo->find($id);
        $next = $repo->findNextYandex((int)$id, $user);
        $prev = $repo->findPrevYandex((int)$id, $user);

        if (!$account->byUser($user)) {
            return $this->redirectToRoute("account_list");
        }
        $mc = [];
        $reservCodes = [];
//        var_dump($next);
        if ($account->getType()->getId() == 3) {
            $mcRepo = $this->getDoctrine()->getRepository(MasterCard::class);
            $mc = $mcRepo->findAllByAccount($account);
            $rcRepo = $this->getDoctrine()->getRepository(YandexReserveCode::class);
            $reservCodes = $rcRepo->findAllActiveByAccount($account);
        }

        return $this->render('Account/account.html.twig', array(
            'user' => $user,
            'mc' => $mc,
            'reservCodes' => $reservCodes,
            'account' => $account,
            'next' => $next,
            'prev' => $prev
        ));


    }

    /**
     * @Route("/add")
     */
    public function addAction()
    {
        return $this->render('AppBundle:YandexAccountController:add.html.twig', array());
    }

    /**
     * @Route("/add_list")
     * @param Request $request
     * @return JsonResponse
     */
    public function add_listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $account = new Account();

        $response = new JsonResponse();
        if (
            $request->request->get("account", null) != null
        ) {
            $acc = json_decode($request->request->get("account"));

            $statusRepo = $this->getDoctrine()->getRepository(Status::class);
            $accountTypeRepo = $this->getDoctrine()->getRepository(AccountType::class);

            $account->setLogin($acc->login);
            $account->setPassword($acc->password);
            $account->setType($accountTypeRepo->find(3));
            $account->setStatus($statusRepo->findValidStatus());
            $account->setCountry("ru");
            $account->setCreator($user);

            $account->setBalance($acc->balance);
            $account->setWallet($acc->wallet);

            $em->persist($account);
            $em->flush();


            $mc = new MasterCard();
            $mc->setNumber($acc->card);
            $mc->setMonth($acc->month);
            $mc->setYear($acc->year);
            $mc->setCvc($acc->cvc);
            $mc->setAccount($account);
            $mc->setCreator($user);

            $em->persist($mc);
            $em->flush();

            foreach ($acc->codes as $cc) {
                $code = new YandexReserveCode();
                $code->setCreator($user);
                $code->setAccount($account);
                $code->setCode($cc);
                $code->setUsed(false);
                $em->persist($code);
                $em->flush();
            }

            $response->setContent(json_encode([
                "status" => "success"
            ]));
        } else {
            $response->setContent(json_encode([
                'status' => "error",
                'text' => "Не все поля дошли до сервера, проверьте ввод данных",
            ]));
        }
        return $response;
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('AppBundle:YandexAccountController:delete.html.twig', array(// ...
        ));
    }

    /**
     * @Route("/add_balance/{id}")
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function add_balanceAction($id, Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Account::class);
        $user = $this->getUser();
        $account = $repo->find($id);

        if ($user != $account->getCreator()) {
            $response->setContent(json_encode([
                'status' => "error",
                'text' => "Нет доступа",
            ]));
            return $response;
        }

        $newBalance = (int)$request->request->get("balance");

        $account->setBalance($account->getBalance() + $newBalance);

        $em->flush();
        $response->setContent(json_encode([
            'status' => "success"
        ]));
        return $response;

    }

}
