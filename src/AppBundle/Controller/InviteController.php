<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Invite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class InviteController
 * @Route("/invite")
 * @package AppBundle\Controller
 */
class InviteController extends Controller
{
    /**
     * @Route("/create", name="create_invite")
     */
    public function createAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Invite::class);

        if ($repo->findActiveCountByUser($user) > 4)
            return $this->redirectToRoute("invite_list");

        $invite = new Invite();
        $invite->setCreator($user);
        $invite->setInvite(md5($user->getId() . time() . rand()));

        $em->persist($invite);
        $em->flush();
        return $this->redirectToRoute("invite_list");

    }

    /**
     * @Route("/", name="invite_list")
     */
    public function showAction()
    {
        $user = $this->getUser();

        $repo = $this->getDoctrine()->getRepository(Invite::class);
        $invites = $repo->findAllByUser($user);


        return $this->render('Invite/list.html.twig', array(
            'user' => $user,
            'invites' => $invites,
        ));
    }

}
