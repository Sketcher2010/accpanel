<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Entity\Invite;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * Class AdminController
 * @Route("/admin-panel")
 * @package AppBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * @Route("/information", name="admin_info")
     */
    public function informationAction()
    {
        $user = $this->getUser();

        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $accountRepo = $this->getDoctrine()->getRepository(Account::class);
        $inviteRepo = $this->getDoctrine()->getRepository(Invite::class);

        $totalUsers = $userRepo->findAll();
        $totalAccounts = $accountRepo->findAll();
        $totalInvites = $inviteRepo->findAll();
        return $this->render('Admin/information.html.twig', array(
            'user' => $user,
            'totalUsers' => count($totalUsers),
            'totalAccounts' => count($totalAccounts),
            'totalInvites' => count($totalInvites),
        ));
    }

}
