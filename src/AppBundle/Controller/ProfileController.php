<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProfileController extends Controller
{
    /**
     * @Route("/profile")
     */
    public function profileAction()
    {
        return $this->render('Profile/profile.html.twig', array(
            // ...
        ));
    }

}
