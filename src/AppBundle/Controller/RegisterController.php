<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Invite;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends Controller
{
    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        if($this->getUser() != null) {
            return $this->redirectToRoute("account_list");
        }
        if($request->query->get("invite") == NULL)
            return $this->redirectToRoute("login");

        $repo = $this->getDoctrine()->getRepository(Invite::class);
        if($repo->findByInvite($request->query->get("invite")) == null)
            return $this->redirectToRoute("login");

        $invite = $repo->findByInvite($request->query->get("invite"));

        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $user->setRegtime(time());
            $user->setLasttime(time());

            $user->setRole("USER");

            $invite->setSubject($user);

            $em->persist($user);
            $em->persist($invite);

            $em->flush();

            return $this->redirectToRoute("login");
        }

        return $this->render('register.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
