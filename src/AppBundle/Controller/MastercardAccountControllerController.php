<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MastercardAccountControllerController extends Controller
{
    /**
     * @Route("/add")
     */
    public function addAction()
    {
        return $this->render('AppBundle:MastercardAccountController:add.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/add_list")
     */
    public function add_listAction()
    {
        return $this->render('AppBundle:MastercardAccountController:add.list.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('AppBundle:MastercardAccountController:delete.html.twig', array(
            // ...
        ));
    }

}
