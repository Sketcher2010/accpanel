<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MailRuAccountControllerController extends Controller
{
    /**
     * @Route("/add")
     */
    public function addAction()
    {
        return $this->render('AppBundle:MailRuAccountController:add.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/add_list")
     */
    public function add_listAction()
    {
        return $this->render('AppBundle:MailRuAccountController:add.list.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('AppBundle:MailRuAccountController:delete.html.twig', array(
            // ...
        ));
    }

}
