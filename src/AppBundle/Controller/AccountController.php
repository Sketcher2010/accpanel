<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AccountType;
use AppBundle\Entity\MasterCard;
use AppBundle\Entity\YandexReserveCode;
use AppBundle\Service\vkService;
use AppBundle\Entity\Account;
use AppBundle\Entity\Status;
use AppBundle\Form\AccountTypeForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class AccountController
 * @package AppBundle\Controller
 * @Route("/accounts")
 */
class AccountController extends Controller
{

    /**
     * @Route("/add", name="add_new_account")
     * @param Request $request
     * @return Response
     */
    public function addAccountAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $account = new Account();
        $form = $this->createForm(AccountTypeForm::class, $account);
        $form->handleRequest($request);


        $statusRepo = $this->getDoctrine()->getRepository(Status::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $account->setStatus($statusRepo->findToCheckStatus());
            $country = "ru";
            switch (substr($account->getLogin(), 0, 2)) {
                case "+3":
                    $country = "ua";
                    break;
            }
            $account->setCountry($country);
            $account->setCreator($user);

            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute("account_list");
        }

        return $this->render('Account/newAccount.html.twig', array(
            'user' => $user,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/add_list", name="add_accounts")
     * @param Request $request
     * @return Response
     */
    public function addAccountsAction(Request $request)
    {
        $user = $this->getUser();

        return $this->render('Account/newAccounts.html.twig', array(
            'user' => $user
        ));
    }

    /**
     * @Route("/add_easy")
     * @param Request $request
     * @return JsonResponse
     */
    public function addAccountFastAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $account = new Account();

        $response = new JsonResponse();

        if (
            $request->request->get("login", null) != null &&
            $request->request->get("password", null) != null
        ) {
            $statusRepo = $this->getDoctrine()->getRepository(Status::class);
            $accountTypeRepo = $this->getDoctrine()->getRepository(AccountType::class);
            $account->setLogin($request->request->get("login"));
            $account->setPassword($request->request->get("password"));
            $account->setType($accountTypeRepo->find(1));
            $account->setStatus($statusRepo->findToCheckStatus());
            $country = "ru";
            switch (substr($account->getLogin(), 0, 2)) {
                case "+3":
                    $country = "ua";
                    break;
            }
            $account->setCountry($country);
            $account->setCreator($user);

            $em->persist($account);
            $em->flush();
            $response->setContent(json_encode([
                "status" => "success"
            ]));
        } else {
            $response->setContent(json_encode([
                'status' => "error",
                'text' => "Не все поля дошли до сервера, проверьте ввод данных",
            ]));
        }
        return $response;
    }

    /**
     * @Route("", name="account_list")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Account::class);
        $currentType = [
            "Все типы",
            "Vk",
            "Od",
            "Yandex",
            "MasterCard",
            "Apple",
            "Mail.Ru"
        ];
        $type = 0;
        if (isset($_GET["type"])) {
            $type = (int)$_GET["type"];
            $accountsActive = $repo->findAllActiveByUserAndType($user, $type);
        } else {
            $accountsActive = $repo->findAllActiveByUser($user);
        }
        $accountsNotActive = $repo->findAllNotActiveByUser($user);
        $error = $request->query->get("error", null);

        return $this->render('Account/index.html.twig', array(
            'user' => $user,
            'accountsActive' => $accountsActive,
            'accountsNotActive' => $accountsNotActive,
            'currentType' => $currentType[$type],
            'error' => $error
        ));
    }


    /**
     * @Route("/{id}", name="account_page")
     * @param $id
     * @return Response
     */
    public function getAccountAction($id)
    {

        $user = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Account::class);
        $account = $repo->find($id);

        if (!$account->byUser($user)) {
            return $this->redirectToRoute("account_list");
        }
        $mc = [];
        $reservCodes = [];

        if ($account->getType()->getId() == 3) {
            $mcRepo = $this->getDoctrine()->getRepository(MasterCard::class);
            $mc = $mcRepo->findAllByAccount($account);
            $rcRepo = $this->getDoctrine()->getRepository(YandexReserveCode::class);
            $reservCodes = $rcRepo->findAllActiveByAccount($account);
        }

        return $this->render('Account/account.html.twig', array(
            'user' => $user,
            'mc' => $mc,
            'reservCodes' => $reservCodes,
            'account' => $account
        ));
    }

    /**
     * @Route("/check_all", name="check_all_accounts")
     * @param vkService $vk
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function checkAllAccounts(vkService $vk)
    {

        $user = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Account::class);
        $account = $repo->findAllActiveByUser($user);

        return $this->render('Account/account.html.twig', array(
            'user' => $user,
            'account' => $account
        ));
    }

    /**
     * @Route("/check/{id}", name="account_check")
     * @param $id
     * @param vkService $vk
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function checkAction($id, vkService $vk)
    {
        $user = $this->getUser();
        $repo = $this->getDoctrine()->getRepository(Account::class);
        $account = $repo->find($id);
        $em = $this->getDoctrine()->getManager();

        if (!$account->byUser($user)) {
            return $this->redirectToRoute("account_list", array("error" => "Недостаточно прав доступа."));
        }

        if ($account->getLastCheckTime() + 60 * 60 * 12 > time() && $user->getRole() != "ADMIN") {
            return $this->redirectToRoute("account_list", array("error" => "Аккаунт можно чекать раз в 12 часов."));
        }

        $res_array = [];

        $auth = $vk->auth($account->getLogin(), $account->getPassword());
        $statusRepo = $this->getDoctrine()->getRepository(Status::class);

        $account->setLastCheckTime(time());

        if (isset($auth->error) && $auth->error == "need_captcha") {

            $vk->setCaptchaSid($auth->captcha_sid);
            $vk->setCaptchaImg($auth->captcha_img);

            $vk->downloadCaptcha($this->get('kernel')->getRootDir());

            $antigate = $vk->getAntigate();

            $antigate->setVerboseMode(true);
            $antigate->setKey("4cdb9f5939ce11ea6ed3fa79c8618166");

            $antigate->setFile($vk->getLocalCaptchaImg());

            if (!$antigate->createTask()) {
                $res_array = array("error" => $antigate->getErrorMessage());
            }

            if (!$antigate->waitForResult()) {
                $res_array = array("error" => "Не получилось решить каптчу. Ошибка: " . $antigate->getErrorMessage());
            } else {
                $vk->setCaptchaKey($antigate->getTaskSolution());
            }

            $auth = $vk->auth($account->getLogin(), $account->getPassword(), true);
        }

        if (isset($auth->access_token)) {
            $token = $auth->access_token;

            $info = $vk->getUserInfo($token);
            $balance = $vk->getUserBalance($token);
            $groupInfo = $vk->getUserGroups($token, $info->response[0]->id);
//            echo "<pre>";
//            var_dump($balance);
//            var_dump($groupInfo->response);
//            echo "</pre>";

            $account->setStatus($statusRepo->findValidStatus());
            $account->setFriends($info->response[0]->counters->friends);
            $account->setSubscribers($info->response[0]->counters->followers);
            $account->setToken($token);
            $account->setAvatar($info->response[0]->photo_50);
            $account->setLink("https://vk.com/id" . $info->response[0]->id);
            $account->setBalance($balance->response->votes);
//            return new Response("");
        }


        if (isset($auth->error) && $auth->error == "need_validation") {
            $account->setStatus($statusRepo->findNeedValidationStatus());
            $res_array = array("error" => "Аккаунт нуждается в верификации.");
        }
        if (isset($auth->error) && $auth->error == "invalid_client") {
            $account->setStatus($statusRepo->findInvalidStatus());
            $res_array = array("error" => "Аккаунт не валидный.");
        }

        $em->persist($account);
        $em->flush();

        return $this->redirectToRoute("account_list", $res_array);
    }

    /**
     * @Route("/edit/{id}")
     */
    public function editAction()
    {
        return $this->render('AppBundle:Account:edit.html.twig', array(// ...
        ));
    }

    /**
     * @Route("/delete/{id}", name="delete_account")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $user = $this->getUser();
        $accountrepo = $this->getDoctrine()->getRepository(Account::class);
        $statusRepo = $this->getDoctrine()->getRepository(Status::class);
        $account = $accountrepo->find($id);
        $em = $this->getDoctrine()->getManager();

        if (!$account->byUser($user)) {
            return $this->redirectToRoute("account_list");
        }

        $account->setStatus($statusRepo->findDeletedStatus());

        $em->persist($account);
        $em->flush();


        return $this->redirectToRoute("account_list");
    }

    /**
     * @Route("/sold/{id}", name="sold_account")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function soldAction($id)
    {
        $user = $this->getUser();
        $accountrepo = $this->getDoctrine()->getRepository(Account::class);
        $statusRepo = $this->getDoctrine()->getRepository(Status::class);
        $account = $accountrepo->find($id);
        $em = $this->getDoctrine()->getManager();

        if (!$account->byUser($user)) {
            return $this->redirectToRoute("account_list");
        }

        $account->setStatus($statusRepo->findSelledStatus());

        $em->persist($account);
        $em->flush();


        return $this->redirectToRoute("account_list");
    }


    /**
     * @Route("/important/{id}", name="important_account")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setImportantAction($id)
    {
        $user = $this->getUser();
        $accountrepo = $this->getDoctrine()->getRepository(Account::class);
        $statusRepo = $this->getDoctrine()->getRepository(Status::class);
        $account = $accountrepo->find($id);
        $em = $this->getDoctrine()->getManager();

        if (!$account->byUser($user)) {
            return $this->redirectToRoute("account_list");
        }

        $account->setStatus($statusRepo->findImportantStatus());

        $em->persist($account);
        $em->flush();


        return $this->redirectToRoute("account_list");
    }

    /**
     * @Route("/notimportant/{id}", name="not_important_account")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setNotImportantAction($id)
    {
        $user = $this->getUser();
        $accountrepo = $this->getDoctrine()->getRepository(Account::class);
        $statusRepo = $this->getDoctrine()->getRepository(Status::class);
        $account = $accountrepo->find($id);
        $em = $this->getDoctrine()->getManager();

        if (!$account->byUser($user)) {
            return $this->redirectToRoute("account_list");
        }
        if ($account->getFriends() != null)
            $account->setStatus($statusRepo->findValidStatus());
        else
            $account->setStatus($statusRepo->findToCheckStatus());
        $em->persist($account);
        $em->flush();


        return $this->redirectToRoute("account_list");
    }

}
