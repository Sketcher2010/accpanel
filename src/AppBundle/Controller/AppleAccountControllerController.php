<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountType;
use AppBundle\Entity\MasterCard;
use AppBundle\Entity\Status;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class AppleAccountControllerController
 * @package AppBundle\Controller
 * @Route("/apple")
 */
class AppleAccountControllerController extends Controller
{
    /**
     * @Route("/add")
     */
    public function addAction()
    {
        return $this->render('AppBundle:AppleAccountController:add.html.twig', array(// ...
        ));
    }


    /**
     * @Route("/add_list_cards")
     * @param Request $request
     * @return JsonResponse
     */
    public function add_list_cardsAction(Request $request)
    {
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($request->request->get("account", null) != null) {
            $data = json_decode($request->request->get("account"));

            $accRepo = $this->getDoctrine()->getRepository(Account::class);
            $mcRepo = $this->getDoctrine()->getRepository(MasterCard::class);

            $account = $accRepo->findByLogin($data->login);
            $mc = $mcRepo->findByNumber($data->card);
            if($account == null || $mc == null) {
                $response->setContent(json_encode([
                    'status' => "error",
                    'text' => "Аккаунты не найдены",
                ]));
            } else {
                if($account->getCreator() == $user && $mc->getCreator() == $user) {
                    $account->setMc($mc);
                    $em->persist($account);
                    $em->flush();

                    $response->setContent(json_encode([
                        'status' => "success"
                    ]));
                } else {
                    $response->setContent(json_encode([
                        'status' => "error",
                        'text' => "Аккаунты должны пренадлежать Вам!",
                    ]));
                }
            }

        } else {
            $response->setContent(json_encode([
                'status' => "error",
                'text' => "Не все поля дошли до сервера, проверьте ввод данных",
            ]));
        }

        return $response;
    }

    /**
     * @Route("/add_list")
     * @param Request $request
     * @return JsonResponse
     */
    public function add_listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $account = new Account();

        $response = new JsonResponse();
        if (
            $request->request->get("account", null) != null
        ) {
            $acc = json_decode($request->request->get("account"));

            $statusRepo = $this->getDoctrine()->getRepository(Status::class);
            $accountTypeRepo = $this->getDoctrine()->getRepository(AccountType::class);
//            $mcRepo = $this->getDoctrine()->getRepository(MasterCard::class);

            $account->setLogin($acc->login);
            $account->setPassword($acc->password);
            $account->setType($accountTypeRepo->find(5));
            $account->setStatus($statusRepo->findValidStatus());
            $account->setCountry("ru");
            $account->setCreator($user);

            if (isset($acc->firstName)) {
                $account->setFirstName($acc->firstName);
                $account->setLastName($acc->lastName);
                $account->setBirthDate($acc->birthDate);
                $account->setSecret($acc->secret);
            }

//            $mc = $mcRepo->findOneBy(["number" => $acc->card]);

//            $account->setMc($mc);

            $em->persist($account);
            $em->flush();

            $response->setContent(json_encode([
                "status" => "success"
            ]));
        } else {
            $response->setContent(json_encode([
                'status' => "error",
                'text' => "Не все поля дошли до сервера, проверьте ввод данных",
            ]));
        }
        return $response;
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('AppBundle:AppleAccountController:delete.html.twig', array(// ...
        ));
    }

}
