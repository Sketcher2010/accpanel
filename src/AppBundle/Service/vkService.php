<?php

namespace AppBundle\Service;

class vkService
{

    private $api_ver = '5.71';
    private $api_url = 'https://api.vk.com/method';

    private $app_id = 3697615;
    private $app_secret = "AlVXZFMUqyrnABp8ncuU";

    private $captcha_sid;
    private $captcha_img;
    private $local_captcha_img;
    private $captcha_key;

    private $antigate;

    /**
     * vkService constructor.
     * @param $antigate
     */
    public function __construct(antigateService $antigate)
    {
        $this->antigate = $antigate;
    }


    public function isURL($url = null)
    {
        return preg_match('/^(http(s)?:\/\/)?(m\.)?vk\.com/is', $url);
    }

    public function ex($url = null)
    {
        $url = explode('/', $url);

        return $url[count($url) - 1];
    }

    public function auth($login, $password, $captcha = false)
    {
        $url = "https://oauth.vk.com/token?grant_type=password&client_id=$this->app_id&client_secret=$this->app_secret&username=$login&password=$password&v=5$this->api_ver";
        if ($captcha) {
            $url .= "&captcha_sid=$this->captcha_sid&captcha_key=$this->captcha_key";
        }
        $ch = curl_init($url);  // инициализируем сеанс
        // устанавливаем параметрЫ для cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// если нам захочется получить содержимое в переменную, а не сразу выводить в браузер
        $result = curl_exec($ch); //выполняем запрос cURL

        return json_decode($result);
    }

    public function getUserInfo($token)
    {
        $url = $this->api_url . "/users.get?fields=photo_50,counters&access_token=" . $token . "&v=" . $this->api_ver;

        $ch = curl_init($url);  // инициализируем сеанс
        // устанавливаем параметрЫ для cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// если нам захочется получить содержимое в переменную, а не сразу выводить в браузер
        $result = curl_exec($ch); //выполняем запрос cURL

        return json_decode($result);
    }

    public function getUserGroups($token, $id)
    {
        $url = $this->api_url . "/groups.get?user_id=" . $id . "&extended=1&filter=admin,editor,moder&fields=members_count,photo_100,is_admin&access_token=" . $token . "&v=" . $this->api_ver;
//        echo $url;
        $ch = curl_init($url);  // инициализируем сеанс
        // устанавливаем параметрЫ для cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// если нам захочется получить содержимое в переменную, а не сразу выводить в браузер
        $result = curl_exec($ch); //выполняем запрос cURL

        return json_decode($result);
    }



    public function getUserBalance($token)
    {
        $url = $this->api_url . "/account.getBalance?access_token=" . $token . "&v=" . $this->api_ver;
//        echo $url;
        $ch = curl_init($url);  // инициализируем сеанс
        // устанавливаем параметрЫ для cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);// если нам захочется получить содержимое в переменную, а не сразу выводить в браузер
        $result = curl_exec($ch); //выполняем запрос cURL

        return json_decode($result);
    }

    public function downloadCaptcha($dir)
    {
        $img = $dir . '/../var/captchas/' . $this->captcha_sid . (time()) . '.jpeg';
        file_put_contents($img, file_get_contents($this->captcha_img));
        $this->local_captcha_img = $img;
    }

    /**
     * @return mixed
     */
    public function getCaptchaSid()
    {
        return $this->captcha_sid;
    }

    /**
     * @param mixed $captcha_sid
     */
    public function setCaptchaSid($captcha_sid)
    {
        $this->captcha_sid = $captcha_sid;
    }

    /**
     * @return mixed
     */
    public function getCaptchaImg()
    {
        return $this->captcha_img;
    }

    /**
     * @param mixed $captcha_img
     */
    public function setCaptchaImg($captcha_img)
    {
        $this->captcha_img = $captcha_img;
    }

    /**
     * @return antigateService
     */
    public function getAntigate()
    {
        return $this->antigate;
    }

    /**
     * @return mixed
     */
    public function getLocalCaptchaImg()
    {
        return $this->local_captcha_img;
    }

    /**
     * @param mixed $local_captcha_img
     */
    public function setLocalCaptchaImg($local_captcha_img)
    {
        $this->local_captcha_img = $local_captcha_img;
    }

    /**
     * @return mixed
     */
    public function getCaptchaKey()
    {
        return $this->captcha_key;
    }

    /**
     * @param mixed $captcha_key
     */
    public function setCaptchaKey($captcha_key)
    {
        $this->captcha_key = $captcha_key;
    }


}