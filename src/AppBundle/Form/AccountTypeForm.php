<?php

namespace AppBundle\Form;

use AppBundle\Entity\Account;
use AppBundle\Entity\AccountType;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountTypeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', TextType::class, [
                'label' => 'Логин',
                'required' => true
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Пароль',
                'required' => true
            ])
//            ->add('link', TextType::class, [
//                'required' => false,
//                'label' => 'Ссылка'
//            ])
            ->add('type', EntityType::class, [
                'class' => AccountType::class,
                'choice_label' => 'text',
                'label' => "Тип",
                'required' => true
            ])
            ->add('submit', SubmitType::class, [
                "attr" => [
                    "class" => 'btn btn-success pull-right'
                ],
                'label' => "Добавить"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Account::class
        ]);
    }
}