var accounts = {
    regex: {
        "vk": /^(.*)(?:\;|\:|\,|\t)(.*)$/gm,
        "yandex": /^(.*)(?:\;|\:|\,|\t)(.*)(?:\;|\:|\,|\t)(.*)(?:\;|\:|\,|\t)(.*)(?:\;|\:|\,|\t)(.*)(?:\;|\:|\,|\t)(.*)(?:\;|\:|\,|\t)(.*)(?:\;|\:|\,|\t)(.*)$/gm,
        "apple": /^(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)((?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*))?$/gm,
        "awy": /^(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)((?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*)(?:\;|\:|\t)(.*))?$/gm
    },
    accountRegex: {
        "vk": /\;|\:|\,|\t/g,
        "yandex": /\;|\:|\,|\t/g,
        "apple": /\;|\:|\t/g,
        "awy": /\;|\:|\t/g
    },

    urls: {
        "vk": "/accounts/add_easy",
        "yandex": "/yandex/add_list",
        "apple": "/apple/add_list",
        "awy": "/apple/add_list_cards"
    },

    selectedSystem: "vk",

    actualRegex: "",
    actualAccountRegex: "",
    actualUrl: "",

    updateSelectedSystem: function () {
        this.selectedSystem = $("#selectedSystem").val();

        this.actualRegex = this.regex[this.selectedSystem];
        this.actualAccountRegex = this.accountRegex[this.selectedSystem];
        this.actualUrl = this.urls[this.selectedSystem];

        if(this.selectedSystem == "yandex") {
            $("#balanceWrapper").show();
        } else {
            $("#balanceWrapper").hide();
        }
    },

    upload: function () {
        var progressBar = $(".progress-bar");
        var progressBarContainer = $(".progress-in-input");
        var textarea = $("textarea");
        var alertContainer = $("#alertContainer");

        var accounts = textarea.val();

        accounts = accounts.match(this.actualRegex);
        dom.showFlex(progressBarContainer);

        var percent = 100/accounts.length;
        var percentsNow = 0;

        for (var i = 0; i < accounts.length; i++) {
            var account = [];
            switch (this.selectedSystem) {
                case "vk":
                    account = this.createVkAccount(accounts[i].split(this.actualAccountRegex));
                    break;
                case "yandex":
                    account = this.createYandexAccount(accounts[i].split(this.actualAccountRegex), $("#balance").val());
                    break;
                case "apple":
                    account = this.createAppleAccount(accounts[i].split(this.actualAccountRegex));
                    break;
                case "awy":
                    account = this.createAppleCardsAccount(accounts[i].split(this.actualAccountRegex));
                    break;
            } //TODO: заменить на унифицированную функцию создания аккаунта
            console.log(account);
            $.ajax({
                url: this.actualUrl,
                method: "POST",
                data: {"account": JSON.stringify(account)},
                success: function (data) {
                    var status = data.status;
                    percentsNow += percent;
                    progressBar.css("width", percentsNow + "%");
                    progressBar.attr("aria-valuenow", percentsNow);

                    console.log(percentsNow);
                    if (status == "error") {
                        var errorText = data.text;
                        alertContainer.html(errorText);
                        alertContainer.show();
                    }
                }
            });
        }
        console.log("finished");
    },

    createVkAccount: function (acc) {
        var account = {};

        account.login = acc[0];
        account.password = acc[1];

        return account;
    },

    createAppleAccount: function (acc) {
        var account = {};

        account.login = acc[0];
        account.password = acc[1];
        account.mail = [];
        account.mail.login = acc[2];
        account.mail.password = acc[3];

        if(acc.length > 3) {
            account.firstName = acc[4];
            account.lastName = acc[5];
            account.birthDate = acc[6];
            account.secret = acc[7];
        }

        return account;
    },

    createAppleCardsAccount: function (acc) {
        var account = {};

        account.login = acc[0];
        account.card = acc[2];

        return account;
    },

    createYandexAccount: function (acc, balance) {
        var account = {};

        account.login = acc[0];
        account.password = acc[1];
        account.wallet = acc[2];
        account.phone = acc[3];
        account.card = acc[4];
        account.month = acc[5].split(/\\|\//g)[0];
        account.year = acc[5].split(/\\|\//g)[1];
        account.cvc = acc[6];
        account.codes = [];
        account.balance = balance;
        var j = 0;
        for(var i = 7; i < acc.length; i++) {
            account.codes[j++] = acc[i];
        }

        return account;
    },

    addYandexBalance: function (id) {
        var balance = $("#newBalance").val();

        $.post("/yandex/add_balance/"+id, {balance: balance}, function (data) {
            var status = data.status;
            if(status == "success") {
                location.reload();
            } else {
                dom.showError(data.text);
            }
        });
    }
};


// TODO: вынести в отдельный файл, еще пригодится это все!
var dom = {
    showFlex: function (elem) {
        elem.css("display", "flex");
    },

    showError: function (text) {
        console.error(text);
    }
};